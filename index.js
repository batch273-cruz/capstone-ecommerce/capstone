

// Customer Class

class Customer {
	constructor(email) {
		this.email = email;
		this.cart = new Cart();
		this.orders = [];
	}

	checkOut() {
		if (this.cart.isEmpty()) {
			console.log('cart cleared')
			return;
		}
		this.cart.computeTotal();

		const order = { 
			products: this.cart.getContents(),
			totalAmount: this.cart.totalAmount
		};
		this.orders.push(order);
		this.cart.clearCartContents();
		console.log('Order Succesfully placed:', order)
	}
}

class Cart {
	constructor() {
		this.contents = [];
		this.totalAmount = 0;
	}

	addToCart(product, quantity) {
		this.contents.push({product, quantity })
	}

	showCartContents() {
		console.log('Cart contents:', this.contents);
	}

	updateProductQuantity(name, newQuantity) {
		const productIndex = this.contents.findIndex(item => newQuantity);

		if (productIndex === -1) {
				console.log('Product not found')
				return;
			}
		this.contents[productIndex].quantity = newQuantity; 
	}

	clearCartContents() {
		this.contents = [];
		this.totalAmount = 0;
	}

	computeTotal() {
		let total = 0;
		for (const item of this.contents) {
			total += item.product.price * item.quantity;
		}
		this.totalAmount = total;
	}

	isEmpty() {
		return this.contents.length === 0;
	}

}

class Product {
	constructor(name, price, isActive) {
		this.name = name;
		this.price = price;
		this.isActive = true;

	}
	archive() {
		return this.isActive = false
	}

	updatePrice(price) {
		this.price.splice(price)
	
	}

}

const john = new Customer('john@mail.com')
const prodA = new Product('soap', 9.99)